from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from joblib import load

sgd_clf = load('sgf_clf.joblib') 
DF = pd.read_csv('pca.csv')
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
df = DF[DF.group == 6]
#df = df.append(DF[DF.group==4])
for group in df.groupby('group'):
    ax.scatter(group[1].x, group[1].y, group[1].z, marker='o',color='#8c564b', lw=0, label=group[0])
ax.legend()
z1 = lambda x,y: (-sgd_clf.intercept_[0]-sgd_clf.coef_[0][0]*x-sgd_clf.coef_[0][1]*y) / sgd_clf.coef_[0][2]
z2 = lambda x,y: (-sgd_clf.intercept_[1]-sgd_clf.coef_[1][0]*x-sgd_clf.coef_[1][1]*y) / sgd_clf.coef_[1][2]
z3 = lambda x,y: (-sgd_clf.intercept_[2]-sgd_clf.coef_[2][0]*x-sgd_clf.coef_[2][1]*y) / sgd_clf.coef_[2][2]
z4 = lambda x,y: (-sgd_clf.intercept_[3]-sgd_clf.coef_[3][0]*x-sgd_clf.coef_[3][1]*y) / sgd_clf.coef_[3][2]
z5 = lambda x,y: (-sgd_clf.intercept_[4]-sgd_clf.coef_[4][0]*x-sgd_clf.coef_[4][1]*y) / sgd_clf.coef_[4][2]
z6 = lambda x,y: (-sgd_clf.intercept_[5]-sgd_clf.coef_[5][0]*x-sgd_clf.coef_[5][1]*y) / sgd_clf.coef_[5][2]
tmp = np.linspace(-6,6,51)
x,y = np.meshgrid(tmp,tmp)
#ax.plot_surface(x, y, z1(x,y),color='#7f7f7f')
#ax.plot_surface(x, y, z2(x,y),color='#7f7f7f')
#ax.plot_surface(x, y, z3(x,y),color='#7f7f7f')
#ax.plot_surface(x, y, z4(x,y),color='#7f7f7f')
#ax.plot_surface(x, y, z5(x,y),color='#7f7f7f')
ax.plot_surface(x, y, z6(x,y),color='#7f7f7f')
plt.show()

