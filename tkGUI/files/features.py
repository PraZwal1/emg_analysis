import numpy as np
import scipy
from tsfel.feature_extraction.features_utils import set_domain

@set_domain("domain", "temporal")
def RMS(signal):
    return np.sqrt(np.sum(np.array(signal) ** 2)/len(signal))

@set_domain("domain", "temporal")
def IAV(signal):
    return np.sum(np.array(abs(signal)))

@set_domain("domain", "temporal")
def MAV(signal):
    return np.sum(np.array(abs(signal)))/len(signal)

@set_domain("domain", "temporal")
def SSI(signal):
    return np.sum(np.array(abs(signal) **2))

@set_domain("domain", "temporal")
def VAR(signal):
    return np.var(signal)

def calc_fft(signal, fs):
    fmag = np.abs(np.fft.fft(signal))
    f = np.linspace(0, fs // 2, len(signal) // 2)
    return f[:len(signal) // 2].copy(), fmag[:len(signal) // 2].copy()

@set_domain("domain", "temporal")
def Peaks(signal):
    f, fmag = calc_fft(signal,1000)
    bp = scipy.signal.find_peaks(fmag, height=0)[0]
    peak_0 = f[bp[0]]
    peak_1 = f[bp[1]]
    peak_2 = f[bp[2]]
    peak_3 = f[bp[3]]
    peak_4 = f[bp[4]]
    return peak_0,peak_1,peak_2,peak_3,peak_4
