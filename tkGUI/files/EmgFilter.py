import scipy as sp
import pandas as pd

def EMG_Filter(DF, sfreq=1000, high_band=20, low_band=450, n_channels=8):
    emg_filtered= pd.DataFrame()
    high_band = high_band/(sfreq/2)
    low_band = low_band/(sfreq/2)
    b1,a1 = sp.signal.butter(4, [high_band,low_band], btype='bandpass')
    for i in range(1,n_channels+1):
        emg_filtered['channel'+ str(i)] = sp.signal.filtfilt(b1,a1,DF['channel'+str(i)])
    return emg_filtered
