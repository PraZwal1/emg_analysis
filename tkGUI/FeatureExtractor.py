import tsfel
import pandas as pd 
import numpy as np
import os 
from files.EmgFilter import EMG_Filter


class FeatureExtractor():
    def __init__(self,csv,n_channels=8,rms=True,mav=True,ssi=True,var=True,n_classes=6):
        self.DF = pd.read_csv(csv)
        self.n_channels = int(n_channels)
        self.rms = rms
        self.ssi = ssi
        self.var = var
        self.mav = mav
        self.n_classes = int(n_classes)
        self.Features = pd.DataFrame()
        self.module_path = os.path.join(os.getcwd(),'features.py') 
        self.json_path = os.path.join(os.getcwd(),'Ft.json') 
        self.DF.rename(columns={"class":"group"}, inplace = True)
        self.DF.drop('label', axis = 1)
        
    def _get_feat_dict(self):
        feat_dict = tsfel.get_features_by_domain(json_path = self.json_path) 
        if not self.rms:
          del feat_dict['temporal']['RMS']
        if not self.mav:
          del feat_dict['temporal']['MAV']
        if not self.ssi:
          del feat_dict['temporal']['SSI']
        if not self.var:
          del feat_dict['temporal']['VAR']
        return feat_dict


    
    def extract(self, test=False):
      feat_dict = self._get_feat_dict()
      if not False:
        for group in range(1,self.n_classes+1):
          DataFrame = self.DF[self.DF.group == group].copy()
          DataFrame = DataFrame.drop('group',axis=1)
          DataFrame_f =  EMG_Filter(DataFrame,high_band=30, low_band=450, n_channels = self.n_channels)
          features = tsfel.time_series_features_extractor(feat_dict, DataFrame_f, fs=1000, window_spliter=True, window_size = 200, features_path= self.module_path)
          features['group'] = group
          self.Features = self.Features.append(features, ignore_index=True) 
          self.Features.to_csv('Extracted.csv',index=False)
          self.extracted = True
        return self.Features
      else:
        for group in range(1,7):
          DataFrame = self.DF[self.DF.group == group].copy()
          DataFrame = DataFrame.drop('group',axis=1)
          DataFrame_f =  EMG_Filter(DataFrame,high_band=30, low_band=450, n_channels = self.n_channels)
          features = tsfel.time_series_features_extractor(feat_dict, DataFrame_f, fs=1000, window_spliter=True, window_size = 200, features_path= self.module_path)
          features['group'] = group
          self.Features = self.Features.append(features, ignore_index=True) 
          self.Features.to_csv('Extracted_test.csv',index=False)
        return self.Features