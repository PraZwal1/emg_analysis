import tkinter as tk
from FeatureExtractor import FeatureExtractor
from tkinter.filedialog import askopenfilename
from tkinter import messagebox
from tkinter import ttk
from Classifiers import Classifiers
from EmgFilter import EMG_Filter
import time

import pandas as pd
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

window = tk.Tk()
window.title('EMG Toolkit')
frame1 = tk.Frame(window)
frame1.pack(side='left')
plt.style.use('dark_background')
fil = tk.IntVar()
rms =tk.BooleanVar(window,False)
var =tk.BooleanVar(window,False)
ssi =tk.BooleanVar(window,False)
mav =tk.BooleanVar(window,True)
rmsbox = tk.Radiobutton(frame1,text= 'RMS', variable=fil,value=0)
rmsbox.select()
varbox = tk.Radiobutton(frame1,text= 'VAR', variable=fil,value=1)
varbox.select()
ssibox = tk.Radiobutton(frame1,text= 'SSI', variable=fil,value=2)
ssibox.select()
mavbox = tk.Radiobutton(frame1,text= 'MAV', variable=fil,value=3)
mavbox.select()
channelBox = ttk.Combobox(frame1, values =[1,2,3,4,5,6,7,8])
channelLabel = tk.Label(frame1, text='No. of Channels')
classBox = ttk.Combobox(frame1, values =[1,2,3,4,5,6])
classLabel = tk.Label(frame1, text= 'No. of Classes')

frame2 = tk.Frame(window)
frame2.pack(side='right')

frame3 = tk.Frame(window)
frame3.pack(side='bottom')

tree = tk.BooleanVar()
sgd = tk.BooleanVar()
knn = tk.BooleanVar()
sgdbox = tk.Checkbutton(frame2,text= 'SGD Classifier', variable=sgd)
sgdbox.select()
treebox = tk.Checkbutton(frame2,text= 'Tree Classifier', variable=tree)
treebox.select()
knnbox = tk.Checkbutton(frame2,text= 'KNN classifier', variable=knn)
knnbox.select()
text_label = tk.Label(frame3)
text_label.pack()



class control:

    def __init__(self):
        self.fileopen = ''
        self.trainer = ''
        self.testfile = ''

    def open_test(self):
        file = askopenfilename()
        if file is not None:
            self.testfile = tk.Label(frame3, text = file)

    def open_file(self):
        file = askopenfilename()
        if file is not None:
            self.fileopen = tk.Label(frame1,text=file)
            graphButton = tk.Button(frame3, text="Show Graph",command= ctl.show_graph)
            self.fileopen.pack()
            graphButton.pack()


    def extract(self):
        if(fil.get()==0):
            rms.set(True)
            mav.set(False)
            ssi.set(False)
            var.set(False)
        if(fil.get()==1):
            rms.set(False)
            mav.set(False)
            ssi.set(False)
            var.set(True)
        if(fil.get()==2):
            rms.set(False)
            mav.set(False)
            ssi.set(True)
            var.set(False)
        if(fil.get()==3):
            rms.set(False)
            mav.set(True)
            ssi.set(False)
            var.set(False)
        ext = FeatureExtractor(csv=self.fileopen['text'],n_channels=channelBox.get(),n_classes=classBox.get() ,mav=mav.get(), rms=rms.get(), ssi=ssi.get(),var=var.get())
        ext.extract()
        messagebox.showinfo('Feature Extraction', 'Feature extraction completed')
    
    def train(self):
        self.trainer = Classifiers(sgd=sgd.get(), knn= knn.get(), tree=tree.get())
        self.trainer.train()
        messagebox.showinfo('Training', 'Training has been completed')

    def show_test(self,classifier='knn'):
        ext = FeatureExtractor(csv=self.testfile['text'],n_channels=channelBox.get(),n_classes=classBox.get() ,mav=mav.get(), rms=rms.get(), ssi=ssi.get(),var=var.get())
        ext.extract(test=True)
        actual , predicted = self.trainer.test(classifier=classifier)
        class_names = ['','hand at rest', 'hand clenched in a fist', 'wrist flexion', 'wrist extension','radial deviations', 'ulnar deviations']
        for i in range (len(predicted)):
            actual_class = str(class_names[actual[i]])
            predicted_class = str(class_names[predicted[i]])
            string = "Actual :{} \n Predicted: {}".format(actual_class,predicted_class)
            text_label['text'] = string
            window.update()
            time.sleep(1)

    def show_scores(self):
        if isinstance(self.trainer,str):
            messagebox.showerror('Error', 'Please Train the model First')
        score = self.trainer.get_test_score(classifier='sgd')
        t = tk.Text(frame3)
        t.insert(tk.END, score)
        t.pack()

    def show_graph(self):
        dataplot = pd.read_csv('Extracted.csv')
        dataplot.rename(columns={'class': 'group'}, inplace=True)
        dataplot = dataplot[dataplot.group == int(classBox.get())]
        if(fil.get()==0):
            if (int(channelBox.get()) == 1):
                data = ['0_RMS']
            if(int(channelBox.get())==2):
                data = ['0_RMS','1_RMS']
            if (int(channelBox.get()) == 3):
                data = ['0_RMS','1_RMS','2_RMS']
            if (int(channelBox.get()) == 4):
                data = ['0_RMS','1_RMS','2_RMS','3_RMS']
            if (int(channelBox.get()) == 5):
                data = ['0_RMS','1_RMS','2_RMS','3_RMS','4_RMS']
            if (int(channelBox.get()) == 6):
                data = ['0_RMS','1_RMS','2_RMS','3_RMS','4_RMS','5_RMS']
            if (int(channelBox.get()) == 7):
                data = ['0_RMS','1_RMS','2_RMS','3_RMS','4_RMS','5_RMS','6_RMS']
            if (int(channelBox.get()) == 8):
                data = ['0_RMS','1_RMS','2_RMS','3_RMS','4_RMS','5_RMS','6_RMS','7_RMS']
        if(fil.get()==1):
            if (int(channelBox.get()) == 1):
                data = ['0_VAR']
            if(int(channelBox.get())==2):
                data = ['0_VAR','1_VAR']
            if (int(channelBox.get()) == 3):
                data = ['0_VAR','1_VAR','2_VAR']
            if (int(channelBox.get()) == 4):
                data = ['0_VAR','1_VAR','2_VAR','3_VAR']
            if (int(channelBox.get()) == 5):
                data = ['0_VAR','1_VAR','2_VAR','3_VAR','4_VAR']
            if (int(channelBox.get()) == 6):
                data = ['0_VAR','1_VAR','2_VAR','3_VAR','4_VAR','5_VAR']
            if (int(channelBox.get()) == 7):
                data = ['0_VAR','1_VAR','2_VAR','3_VAR','4_VAR','5_VAR','6_VAR']
            if (int(channelBox.get()) == 8):
                data = ['0_VAR','1_VAR','2_VAR','3_VAR','4_VAR','5_VAR','6_VAR','7_VAR']
        if(fil.get()==2):
            if (int(channelBox.get()) == 1):
                data = ['0_SSI']
            if(int(channelBox.get())==2):
                data = ['0_SSI','1_SSI']
            if (int(channelBox.get()) == 3):
                data = ['0_SSI','1_SSI','2_SSI']
            if (int(channelBox.get()) == 4):
                data = ['0_SSI','1_SSI','2_SSI','3_SSI']
            if (int(channelBox.get()) == 5):
                data = ['0_SSI','1_SSI','2_SSI','3_SSI','4_SSI']
            if (int(channelBox.get()) == 6):
                data = ['0_SSI','1_SSI','2_SSI','3_SSI','4_SSI','5_SSI']
            if (int(channelBox.get()) == 7):
                data = ['0_SSI','1_SSI','2_SSI','3_SSI','4_SSI','5_SSI','6_SSI']
            if (int(channelBox.get()) == 8):
                data = ['0_SSI','1_SSI','2_SSI','3_SSI','4_SSI','5_SSI','6_SSI','7_SSI']
        if(fil.get()==3):
            if (int(channelBox.get()) == 1):
                data = ['0_MAV']
            if(int(channelBox.get())==2):
                data = ['0_MAV','1_MAV']
            if (int(channelBox.get()) == 3):
                data = ['0_MAV','1_MAV','2_MAV']
            if (int(channelBox.get()) == 4):
                data = ['0_MAV','1_MAV','2_MAV','3_MAV']
            if (int(channelBox.get()) == 5):
                data = ['0_MAV','1_MAV','2_MAV','3_MAV','4_MAV']
            if (int(channelBox.get()) == 6):
                data = ['0_MAV','1_MAV','2_MAV','3_MAV','4_MAV','5_MAV']
            if (int(channelBox.get()) == 7):
                data = ['0_MAV','1_MAV','2_MAV','3_MAV','4_MAV','5_MAV','6_MAV']
            if (int(channelBox.get()) == 8):
                data = ['0_MAV','1_MAV','2_MAV','3_MAV','4_MAV','5_MAV','6_MAV','7_MAV']
        dataplot.plot(y=data,subplots=True,figsize=(10,5))
        plt.show()

         
    
    



ctl = control()
openButton = tk.Button(frame1, text = 'Open CSV', command = ctl.open_file)
extractButton = tk.Button(frame1, text = 'Extract Features', command = ctl.extract)
trainButton = tk.Button(frame2, text="Train", command= ctl.train)
openTestButton = tk.Button(frame3,text = "Open test File", command=ctl.open_test)
testButton = tk.Button(frame3, text="Test results", command = ctl.show_test)
scoreButton = tk.Button(frame3, text="Accuracy Score",command = ctl.show_scores)
channelBox.current(7)
classBox.current(5)
channelLabel.pack()
channelBox.pack()
classLabel.pack()
classBox.pack()
rmsbox.pack()
varbox.pack()
ssibox.pack()
mavbox.pack()
openButton.pack()
extractButton.pack()
sgdbox.pack()
treebox.pack()
knnbox.pack()
trainButton.pack()
openTestButton.pack()
testButton.pack()
scoreButton.pack()
window.mainloop() 