import pandas as pd #for handling data
import numpy as np #for handling data matrix
import matplotlib.pyplot as plt #for plotting
import matplotlib #for plotting
from sklearn.preprocessing import StandardScaler #for scaling features
from sklearn.model_selection import train_test_split #for creating train and test sets
from sklearn.linear_model import SGDClassifier #classifier 
from sklearn.metrics import precision_score, recall_score, confusion_matrix 
from sklearn.tree import DecisionTreeClassifier 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import VotingClassifier
from joblib import dump, load



class Classifiers():
    def __init__(self,sgd=True, knn =True, tree=True,test=False):
        self.sgd = sgd
        self.knn = knn
        self.tree = tree
        if not test:
            self.Features = pd.read_csv('files/Extracted.csv')
        else:
            self.Features = pd.read_csv('files/Extracted_test.csv')
        self.Features = self.Features.sample(frac=1, random_state=42)
        self.sgd_c = False
        self.knn_c = False
        self.tree_c = False
    
    def _get_matrix(self):
        _ = self.Features.copy()
        y = _.pop('group').values
        X = _.values
        return X,y

    def _matrix_scale_split(self):
        X, y = self._get_matrix()
        std_scaler = StandardScaler()
        X = std_scaler.fit_transform(X)
        return X,y
        #X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, stratify=y,random_state =42)
        #if train:
            #return X_train, y_train
       # else:
        #    return X_test, y_test

    def train(self, test=False):
        if test:
            return
        X_train, y_train = self._matrix_scale_split()
        if self.sgd:    
            sgd_clf = SGDClassifier(loss='log', alpha=0.001, max_iter=5000, penalty='l1', tol=1e-5)
            sgd_clf.fit(X_train,y_train)
            dump(sgd_clf, 'files/sgd_clf.joblib')

        if self.tree:
            tree_clf = DecisionTreeClassifier(criterion='gini',max_depth=9,min_samples_leaf=5,min_samples_split=15, random_state=42)
            tree_clf.fit(X_train,y_train)
            dump(tree_clf,'files/tree_clf.joblib')
        
        if self.knn:
            knn_clf = KNeighborsClassifier(n_neighbors=17,p=1)
            knn_clf.fit(X_train,y_train)
            dump(knn_clf,'files/knn_clf.joblib')
        
    def test(self,classifier):
        X_test, y_test = self._matrix_scale_split()
        if classifier =="knn":
            knn_clf = load('files/knn_clf.joblib')
            results = knn_clf.predict(X_test)
            return y_test, results
        if classifier =="sgd":
            sgd_clf = load('files/sgd_clf.joblib')
            results = sgd_clf.predict(X_test)
            return y_test, results
        if classifier == "tree":
            tree_clf = load('files/tree_clf.joblib')
            results = tree_clf.predict(X_test)
            return y_test, results
        else: 
            raise('Please select a valid classifier.')
    
    def get_test_score(self,classifier):
        y_test, result = self.test(classifier=classifier)
        score = 'Precision score: {}, Recall score: {}'.format(precision_score(y_test, result, average='weighted'),recall_score(y_test, result, average='weighted'))
        return score
