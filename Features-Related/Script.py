#importing libraries
import pandas as pd
import numpy as np
import tsfel
import os
import json
import matplotlib.pyplot as plt
from EmgFilter import EMG_Filter

#setting up paths for features
module_path = os.path.join(os.getcwd(),'features.py') 
json_path = os.path.join(os.getcwd(),'features.json')

#loading required tsfel features dictionary from our customised json file
with open('feat_dict1.json', mode='rt') as f:
    dict_feact = json.load(f)

#loading custom feaures dictionary that we implemented 
feat_dict = tsfel.get_features_by_domain('temporal', json_path)

#merging two dictionaries
feat_dict.update(dict_feact)

#creating blank dataframe for features
Features = pd.DataFrame()

#selecting the dataset
DF = pd.read_csv('EMG-data.csv')

#renaming class to group to avoid name space clash 
DF.rename(columns={"class":"group"}, inplace = True)

#Extracting Features
for label in range(1,37):
    df = DF[DF.label== label].copy()
    for group in range(0,8):
        Dataframe = df[df.group == group].copy()
        Dataframe = Dataframe[['channel1', 'channel2', 'channel3', 'channel4', 'channel5', 'channel6', 'channel7', 'channel8']]
        Dataframe = EMG_Filter(Dataframe,high_band=30, low_band=450, n_channels = 8)
        features = tsfel.time_series_features_extractor(feat_dict, Dataframe, fs=1000, window_spliter=True, window_size = 50, features_path= module_path)
        features['label'] = label
        features['group'] = group
        Features = Features.append(features, ignore_index=True)

#copying features to csv
Features.to_csv('sEMG_Features.csv')