import numpy as np
from tsfel.feature_extraction.features_utils import set_domain

@set_domain("domain", "temporal")
def RMS(signal):
    return np.sqrt(np.sum(np.array(signal) ** 2)/len(signal))

@set_domain("domain", "temporal")
def IAV(signal):
    return np.sum(np.array(abs(signal)))

@set_domain("domain", "temporal")
def MAV(signal):
    return np.sum(np.array(abs(signal)))/len(signal)

@set_domain("domain", "temporal")
def SSI(signal):
    return np.sum(np.array(abs(signal) **2))

@set_domain("domain", "temporal")
def VAR(signal):
    return np.var(signal)
