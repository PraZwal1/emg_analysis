import os
import pandas as pd 
import numpy as np 
from sklearn.preprocessing import StandardScaler 
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score 
from sklearn.tree import DecisionTreeClassifier 
from sklearn.neighbors import KNeighborsClassifier
from joblib import dump, load
import errno


class Trainer:
    def __init__(self,sgd=True, knn =True, tree=True):
        self.sgd = sgd
        self.knn = knn
        self.tree = tree
        self.Features = self._get_df()
    
    def _get_df(self):
        df_path = os.path.join(os.getcwd(),'files/extracted.csv')
        if not os.path.isfile(df_path):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), df_path)
        DF = pd.read_csv(df_path)
        return DF
    
    def _get_matrix(self):
        _ = self.Features.copy()
        _ = _.sample(frac=1,random_state=42)
        y = _.pop('group').values
        X = _.values
        return X,y
    
    def _matrix_preprocess(self,split,test_ratio):
        X,y = self._get_matrix()
        if split == True:
            X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=test_ratio, stratify=y,random_state =42)
            std_scaler = StandardScaler()
            X_train = std_scaler.fit_transform(X_train)
            X_test = std_scaler.fit_transform(X_test)
            return X_train, y_train, X_test, y_test
        else:
            std_scaler = StandardScaler()
            X = std_scaler.fit_transform(X)
            return X ,y
    
    def train(self,model_name,split=True,test_ratio = 0.2):
        X_train, y_train, self.X_test ,self.y_test = self._matrix_preprocess(split=split, test_ratio=test_ratio)
        if self.sgd:    
            self.sgd_clf = SGDClassifier(loss='log', alpha=0.001, max_iter=5000, penalty='l1', tol=1e-5)
            self.sgd_clf.fit(X_train,y_train)
            dump(self.sgd_clf, 'files/{}_sgd.joblib'.format(model_name))

        if self.tree:
            self.tree_clf = DecisionTreeClassifier(criterion='gini',max_depth=9,min_samples_leaf=5,min_samples_split=15, random_state=42)
            self.tree_clf.fit(X_train,y_train)
            dump(self.tree_clf,'files/{}_tree.joblib'.format(model_name))
        
        if self.knn:
            self.knn_clf = KNeighborsClassifier(n_neighbors=17,p=1)
            self.knn_clf.fit(X_train,y_train)
            dump(self.knn_clf,'files/{}_knn.joblib'.format(model_name))
    
    def test(self):
        accuracy = []
        if self.sgd:
            y_pred = self.sgd_clf.predict(self.X_test)
            score = accuracy_score(self.y_test,y_pred)
            accuracy.append('SGD Classifier: {}'.format(score))
        if self.tree:
            y_pred = self.tree_clf.predict(self.X_test)
            score = accuracy_score(self.y_test,y_pred)
            accuracy.append('Decision Tree Classifier: {}'.format(score))
        if self.knn:
            y_pred = self.knn_clf.predict(self.X_test)
            score = accuracy_score(self.y_test,y_pred)
            accuracy.append('KNN Classifier: {}'.format(score))
        
        string = ''
        for ele in accuracy:
            string += ele
            string += '\n'
        return string 
