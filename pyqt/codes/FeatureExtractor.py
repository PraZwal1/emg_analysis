import tsfel
import pandas as pd 
import numpy as np 
import os 
import json
import errno

class Feature_Extractor:
    """ This module Extracts the Features from Raw EMG Dataset. """
    def __init__(self,rms=True,mav=True,ssi=True,var=True,
    max_freq=True,wavelet_abs_mean=True):
        """Initialization Class.
        Parameters:

        rms: (bool) Set true to calculate RMS of the signal.
        mav: (bool) Set true to calculate MAV of the signal.
        ssi: (bool) Set true to calculate SSI of the signal.
        var: (bool) Set true to calculate Variance of the signal.
        max_freq: (bool) Set true to calculate Maximum Frequency of the signal.
        wavelet_abs_mean: (bool) Set true to calculate Wavelet Absolute Mean of the signal.
        """
        self.rms = rms
        self.ssi = ssi
        self.mav = mav
        self.var = var
        self.max_freq = max_freq
        self.wavelet_abs_mean = wavelet_abs_mean
        self.Features = pd.DataFrame()
        
        def _get_paths():
            """ This functions gets the path of required json file.""" 
            module_path = os.path.join(os.getcwd(),'codes/features.py') 
            json_path = os.path.join(os.getcwd(),'codes/Ft.json')
            if not os.path.isfile(module_path):
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), module_path)
            if not os.path.isfile(json_path):
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), json_path)
            return module_path, json_path
        self.module_path, self.json_path = _get_paths()
    
    def _get_feat_dict(self):
        """This functions gets the feature dictionary.
        Returns:
        Features Dictionary
        """
        with open(self.json_path) as jfile:
          feat_dict = json.load(jfile)
        if not self.rms:
          del feat_dict['temporal']['RMS']
        if not self.mav:
          del feat_dict['temporal']['MAV']
        if not self.ssi:
          del feat_dict['temporal']['SSI']
        if not self.var:
          del feat_dict['temporal']['VAR']
        if not self.wavelet_abs_mean:
          del feat_dict['spectral']['Wavelet absolute mean']
        if not self.max_freq:
          del feat_dict['spectral']['Maximum frequency']
        return feat_dict

    def _get_dataframe(self,csv):
      """This functions gets Dataframe.
      Paramaters:
      csv:(str) The csv of signals whose features are to be extracted.
      Returns:
      DataFrame of signals.
      """
      DF = pd.read_csv(csv)
      if DF.label.any():
        DF.drop('label',axis=1, inplace=True)
      DF.drop('time',axis=1, inplace=True)
      DF.rename(columns={'class':'group'}, inplace = True)
      return DF

  
    def extract(self, csv, n_channels, n_classes, name):
      """This function extracts features from the provided dataset.
        Parameters:
      
        csv:(str) The csv of signals whose features are to be extracted.
        n_channels:(int) Number of channels of the signal.
        n_classes:(int) Number of prediction classes. 
        name:(str) Name for the feature file."""
      DF = self._get_dataframe(csv)
      feat_dict = self._get_feat_dict()
      for group in range(1,n_classes+1):
        Dataframe = DF[DF.group == group].copy()
        Dataframe.pop('group')
        features = tsfel.time_series_features_extractor(feat_dict, Dataframe, fs=1000, window_spliter=True, window_size = 200, features_path= self.module_path)
        features['group'] = group
        self.Features = self.Features.append(features, ignore_index=True)
      self.Features.to_csv(name, index=False)

