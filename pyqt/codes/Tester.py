from codes.FeatureExtractor import Feature_Extractor
from joblib import dump, load
from sklearn.metrics import accuracy_score 
from sklearn.preprocessing import StandardScaler

class Tester(Feature_Extractor):
    def _get_model(self, name):
        model = load(name)
        return model

    def _get_matrix(self):
        _ = self.Features.copy()
        _ = _.sample(frac=1,random_state=42)
        y = _.pop('group').values
        X = _.values
        scaler = StandardScaler()
        X = scaler.fit_transform(X)
        return X,y


    def test(self, name):
        model = self._get_model(name)
        X,y = self._get_matrix()
        results = model.predict(X)
        classes = ['','hand at rest','hand clenched in a fist','wrist flexion', 'wrist extension','radial deviations', 'ulnar deviations']
        actuals = [classes[y[i]] for i in range(len(y))]
        predicted = [classes[results[i]] for i in range(len(results))]
        score = accuracy_score(y,results)
        return actuals, predicted, str(score)