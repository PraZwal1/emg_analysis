from time import sleep
import numpy as np
import pandas as pd
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QApplication, QLabel
from MLTrainer import train_data, test_now
from PyQt5 import QtWidgets, QtCore, uic
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication
import os
import webbrowser

from random import randint
class record_emg(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(record_emg, self).__init__(*args, **kwargs)
        uic.loadUi('RecordEMGsignal.ui', self)
class about_dl(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(about_dl, self).__init__(*args, **kwargs)
        uic.loadUi('AboutDesignlab.ui', self)
class about_us(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(about_us, self).__init__(*args, **kwargs)
        uic.loadUi('AboutUs.ui', self)

class about_drl(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(about_drl, self).__init__(*args, **kwargs)
        uic.loadUi('AboutDRL.ui', self)
class Simulate_emg(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(Simulate_emg, self).__init__(*args, **kwargs)
        uic.loadUi('simulate.ui', self)
        self.load_data()
        self.simulateButton.clicked.connect(self.simulate_button_pressed)
        self.include=[]
        self.gplot =  self.graphWidget.plot()
        self.graphWidget.hideAxis('left')
        self.graphWidget.hideAxis('bottom')
        

    def load_data(self):
        dd=pd.read_csv('1.txt',sep='\t')
        frame=[]
        self.channels=['channel1','channel2','channel3','channel4','channel5','channel6','channel7','channel8']
        for name,group in dd.groupby('class'):
            if (name==0):
                for channel in self.channels:
                    group[channel]=0
                frame.append(group)
            else:
                frame.append(group)
        df=pd.concat(frame)
        df.sort_values('time', ascending=True,inplace=True)
        self.data_set=[]
        dc=pd.DataFrame()
        self.cl=df['class'].tolist()
        for channel in self.channels:
            dc[channel]=df[channel]*10000
            data_single=dc[channel].tolist()
            self.data_set.append(data_single)

    def run(self):
        self.timer = QtCore.QTimer()
        self.timer.setInterval(50)
        self.timer.timeout.connect(self.update_plot_data)
        self.timer.start()
        
    def simulate_button_pressed(self):
        
        self.include=[]
        for i in range(1,9):
            exec("self.channel_name_active_info_"+str(i)+".move(1000,1000)")
            
        
        if self.channel1.isChecked()==True:
            self.include.append('channel1')
        if self.channel2.isChecked()==True:
            self.include.append('channel2')
        if self.channel3.isChecked()==True:
            self.include.append('channel3')
        if self.channel4.isChecked()==True:
            self.include.append('channel4')
        if self.channel5.isChecked()==True:
            self.include.append('channel5')
        if self.channel6.isChecked()==True:
            self.include.append('channel6')
        if self.channel7.isChecked()==True:
            self.include.append('channel7')
        if self.channel8.isChecked()==True:
            self.include.append('channel8')
        
        self.nPlots = len(self.include)
        point=[[210,450,0],[210,520,-320],[210,560,-170],[210,570,-130],[210,580,-105],[210,590,-90],[210,600,-80],[210,610,-70]]
        for i in range(1,9):
            if len(self.include)==i:
                x=point[i-1][0]
                y=point[i-1][1]
                delta=point[i-1][2]
        self.Number_of_channel_label.setText(str(self.nPlots))
        self.Number_of_point_label.setText(str(len(self.cl)))
        
        for i in range(len(self.include)):
            j=0
            for channel in self.channels:
                j=j+1
                
                if self.include[i]==channel:
                    
                    exec("self.channel_name_active_info_"+str(j)+".move(x,y)")
            y=y+delta
           
            
        self.nSamples = 2000
        self.curves=[]
        self.graphWidget.clear()
        
        for idx in range(self.nPlots):
            curve = pg.PlotCurveItem(pen=(idx,self.nPlots*1.3))
            self.graphWidget.addItem(curve)
            curve.setPos(0,idx*12)
            self.curves.append(curve)
        self.graphWidget.setYRange(-6, self.nPlots*12)
        self.graphWidget.setXRange(0, self.nSamples)
        
        self.p=0
        self.ptr = 0
        self.run()
    def update_plot_data(self):
        ac=[]
        
        for channel in self.channels:
            for i in range(len(self.include)):
           
                if self.include[i]==channel:
                    
                    ab=self.data_set[i][self.p:self.p+3000]
                    a=np.array(ab)
                    ac.append(a)
        for i in range(len(self.include)):
            self.curves[i].setData(ac[i])
        if (self.p>60000):
            self.p=-1000
        else:
            self.p=self.p+100
        sleep(0.01)
        self.graphWidget.setTitle("class: %s" %self.cl[self.p+500])
    


class MainWindow(QtWidgets.QMainWindow):
    
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        uic.loadUi('gui.ui', self)
        self.About_us_button.clicked.connect(self.about_design_lab)
        self.About_us_button_2.clicked.connect(self.about_drl)
        self.Simulate_button.clicked.connect(self.simulate_emg_signal)
        self.Record_emg_button.clicked.connect(self.record_emg_signal)
        self.Train_machine_learning_button.clicked.connect(self.train_data)
        self.Test_emg_signal.clicked.connect(self.test_data)
        self.AboutApplication_button.clicked.connect(self.about_us)
        self.simulationButton.clicked.connect(self.simulation)

    def simulate_emg_signal(self):
        self.simulate = Simulate_emg()
        self.simulate.show()
    def about_design_lab(self):
        #self.about = about_dl()
        #self.about.show()
        webbrowser.open('http://designlab.ku.edu.np/about-us/')
    def about_drl(self):
        #self.about = about_drl()
        #self.about.show()
        webbrowser.open('http://dlrlab.ku.edu.np/?page_id=4793')

    def record_emg_signal(self):
        self.record = record_emg()
        self.record.show()
    def train_data(self):
        self.train_data = train_data()
        self.train_data.show()

    def test_data(self):
        self.test_data = test_now()
        self.test_data.show()

    def about_us(self):
        self.about_us = about_us()
        self.about_us.show()
    
    def simulation(self):
        webbrowser.open('https://minusha113.github.io/prostheticarm/')
     
def main():
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())



if __name__ == '__main__':         
    main()
"""app = QtWidgets.QApplication(sys.argv)
w = MainWindow()
w.show()
sys.exit(app.exec_())"""
