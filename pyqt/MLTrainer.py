
from PyQt5 import QtWidgets,uic
from PyQt5 import QtGui
from codes.FeatureExtractor import Feature_Extractor
from codes.Classifier import Trainer
from codes.Tester import Tester
from PyQt5.QtWidgets import QTableWidgetItem, QHeaderView

class train_data(QtWidgets.QDialog):
    def __init__(self, *args, **kwargs):
        super(train_data,self).__init__(*args, **kwargs)
        uic.loadUi('MachineLearningTraining.ui', self)
        self.startnowButton.clicked.connect(self.start_now)

    def start_now(self):
        self.start_now = start_now()
        self.start_now.show()

class start_now(QtWidgets.QWizard):
    def __init__(self, *args, **kwargs):
        super(start_now,self).__init__(*args, **kwargs)
        uic.loadUi('MLTrainerWizard.ui', self)
        self.openButton.clicked.connect(self.open_file)
        self.extractButton.clicked.connect(self.extracter)
        self.split_yes.clicked.connect(self.enable_ratio)
        self.split_no.clicked.connect(self.disable_ratio)
        self.trainButton.clicked.connect(self.train)
        self.testButton.clicked.connect(self.show_test_popup)
        
          
    def open_file(self):
        self.name = QtGui.QFileDialog.getOpenFileName(self, 'Open File')
        self.openLabel.setText(self.name[0])  
    
    def show_Ft_popup_1(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Feature Extraction')
        msg.setText('Feature Extraction Started! Please be patient and wait for completion messege.')
        msg.setIcon(QtWidgets.QMessageBox.Information)
        x = msg.exec_()

    def show_Ft_popup_2(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Feature Extraction')
        msg.setText('Feature Extraction Completed! Go ahead and train your model now.')
        msg.setIcon(QtWidgets.QMessageBox.Information)
        x = msg.exec_()


    def extracter(self):
        rms = self.rmsbox.isChecked()
        ssi = self.ssibox.isChecked()
        mav = self.mavbox.isChecked()
        var = self.varbox.isChecked()
        max_freq = self.maxfreqbox.isChecked()
        wavelet_abs_mean = self.wavabsbox.isChecked()
        n_channels = self.channelbox.value()
        n_classes = self.classbox.value()
        self.feat_ext = Feature_Extractor(rms=rms, ssi=ssi, mav=mav, var=var, max_freq=max_freq, wavelet_abs_mean=wavelet_abs_mean)
        self.show_Ft_popup_1()
        try:
            self.feat_ext.extract(csv=self.name[0], n_channels=n_channels,n_classes=n_classes, name='files/extracted.csv')
            self.show_Ft_popup_2()
        except:
            self.error1()
        

    def error1(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Error!')
        msg.setText("There was an error. Please make sure you selected a valid csv file.")
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        x = msg.exec_()
    def enable_ratio(self):
        self.ratiolabel.setEnabled(True)
        self.ratiobox.setEnabled(True)
        self.testButton.setEnabled(True)
    
    def disable_ratio(self):
        self.ratiolabel.setEnabled(False)
        self.ratiobox.setEnabled(False)
        self.testButton.setEnabled(False)

    def show_T_popup_1(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Model Training')
        msg.setText('Training Started!')
        msg.setIcon(QtWidgets.QMessageBox.Information)
        x = msg.exec_()

    def show_T_popup_2(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Model Training')
        msg.setText('Training Completed!')
        msg.setIcon(QtWidgets.QMessageBox.Information)
        x = msg.exec_()

    def train(self):
        sgd = self.sgdbox.isChecked()
        tree = self.dtbox.isChecked()
        knn = self.knnbox.isChecked()
        split = self.split_yes.isChecked()
        model_name = self.modelName.text()
        split_ratio = self.ratiobox.value()
        self.trainer = Trainer(sgd=sgd, knn=knn, tree=tree)
        self.show_T_popup_1()
        try:
            self.trainer.train(model_name, split, split_ratio)
            self.show_T_popup_2()
        except:
            self.error2()

        
    def error2(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Error!')
        msg.setText("Plese make sure you extracted features or selected a training algorithm")
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        x = msg.exec_()
    def show_test_popup(self):
        msg = QtWidgets.QMessageBox()        
        text = self.trainer.test() 
        msg.setWindowTitle('Test Results')
        msg.setText(text)
        msg.setIcon(QtWidgets.QMessageBox.Information)
        x = msg.exec_()

class test_now(QtWidgets.QDialog):
    def __init__(self, *args, **kwargs):
        super(test_now,self).__init__(*args, **kwargs)
        uic.loadUi('Tests.ui', self)
        self.opencsv.clicked.connect(self.open_csv)
        self.openmodel.clicked.connect(self.open_model)
        self.testbutton.clicked.connect(self.test_button)

    def open_csv(self):
        self.name1 = QtGui.QFileDialog.getOpenFileName(self, 'Open File')
        self.csvlabel.setText(self.name1[0])  
    
    def open_model(self):
        self.name2 = QtGui.QFileDialog.getOpenFileName(self, 'Open File')
        self.modellabel.setText(self.name2[0])  

    def test_button(self):
        try:
            rms = self.rmsbox.isChecked()
            ssi = self.ssibox.isChecked()
            mav = self.mavbox.isChecked()
            var = self.varbox.isChecked()
            n_channels = self.channelbox.value()
            n_classes = self.classbox.value()
            self.show_Ft_popup_1()
            self.tester = Tester(rms=rms, ssi=ssi, mav=mav, var=var, max_freq=False, wavelet_abs_mean=False)
            self.tester.extract(csv=self.name1[0], n_channels=n_channels,n_classes=n_classes, name='files/train_ext.csv')
            try:
                actual, predicted, score = self.tester.test(self.name2[0])
                test = test_table()
                test.get_table(actual,predicted,score)
                x = test.exec_()
            except:
                self.error2()
        except:
            self.error1()
    
    def show_Ft_popup_1(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Test From CSV File')
        msg.setText('Please Wait')
        msg.setIcon(QtWidgets.QMessageBox.Information)
        x = msg.exec_()
    def error1(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Error!')
        msg.setText("Please Select csv, model, channels and features before clicking this button")
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        x = msg.exec_()

    def error2(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Error!')
        msg.setText("There was an error Please check the features, channels or classes for the model")
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        x = msg.exec_()

class test_table(QtWidgets.QDialog):
    def __init__(self, *args, **kwargs):
        super(test_table,self).__init__(*args, **kwargs)
        uic.loadUi('TestTable.ui', self)

    def get_table(self,actual,predicted,score):
        self.accuracylabel.setText(score)
        self.tableWidget.setRowCount(len(actual))   
        self.tableWidget.setColumnCount(2)  
        for i in range(0,len(actual)):
            self.tableWidget.setItem(i,0, QTableWidgetItem(actual[i]))
            self.tableWidget.setItem(i,1, QTableWidgetItem(predicted[i]))
        self.tableWidget.horizontalHeader().setStretchLastSection(True) 
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch) 
